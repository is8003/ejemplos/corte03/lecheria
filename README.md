# La lechería

Una fábrica de lácteos cuenta con una sofisticada planta donde diferentes
máquinas, controladas por operarios humanos, son utilizadas para sacar los
productos al mercado. Cada máquina, identificada por un único número, requiere
para ser controlada una o varias habilidades específicas para ella. Las
máquinas operan 24 horas y para cada turno (día o noche) se sabe si tiene un
operario asignado o no. La fábrica cuenta con un grupo de operarios entrenados,
identificados por su nombre (se garantiza que no hay tocayos en la fábrica), y
cada uno de ellos cuenta con una o varias habilidades específicas relacionadas
con el manejo de las máquinas. Un operario sólo trabaja un turno: dia o noche,
es decir no trabaja 24 horas, solo 12.

1. Defina claramente las estructuras de datos requeridas (arreglos, matrices
   y/o estructuras).

1.  Construya una función que realice la asignación, de un día completo, de
    operarios a las máquinas. Un operario será asignado a una máquina si está
    libre y tiene todas las habilidades requeridas para controlarla. La función
    debe generar un arreglo que contenga la información que relacione la
    máquina con el turno y el operario asignado. La función retorna la cantidad
    de asignaciones realizadas. Tenga en cuenta que una máquina que trabaja 24
    horas requerirá dos operarios.
