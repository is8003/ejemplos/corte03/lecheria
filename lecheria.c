#include <stdio.h>
#include <stdbool.h>
#define MAX_HAB 3
#define CANT_MAQ 2
#define CANT_OPS 4

typedef struct {
	char nombre[20];
	bool habs[MAX_HAB];
	bool disp;
} Operario;

typedef struct {
	unsigned id;
	bool reqs[MAX_HAB];
	bool uso[2]; // Posición 0: noche, posición 1: dia
} Maquina;

typedef struct {
	Operario * op;
	Maquina * maq;
	bool diurno; // false: nocturno, true: diurno
} Asignacion;

unsigned asignar(Operario ops[], unsigned nOp, Maquina maqs[], unsigned nMaq,
		Asignacion asig[]);

int main(void){
	Maquina maqs[CANT_MAQ] = {
		{10, {true, false, true}, {false, false}},
		{20, {false, true, true}, {false, false}}
	};

	Operario ops[CANT_OPS] = {
		{ "Gabriel", {true, false, true}, true},
		{ "María", {true, true, true}, true},
		{ "Isabel", {false, true, true}, true},
		{ "Carlos", {true, false, true}, true}
	};

	Asignacion asig[CANT_MAQ * 2];

	unsigned cantAsig = asignar(ops, CANT_OPS, maqs, CANT_MAQ, asig);

	for (size_t i = 0; i < cantAsig; i++){
		printf("\nOperador(a): %s\n", asig[i].op->nombre);
		printf("Máquina No.: %u\n", asig[i].maq->id);
		printf("Turno diurno: %u\n", asig[i].diurno);
	}
}

unsigned asignar(Operario ops[], unsigned nOp, Maquina maqs[], unsigned nMaq,
		Asignacion asig[]){
	unsigned iAsig = 0;

	for (size_t t = 0; t < 2; t++){
		for (size_t i = 0; i < nMaq; i++){
			for (size_t j = 0; j < nOp; j++){
				if(ops[j].disp == true && maqs[i].uso[t] == false){
					bool capaz = true;

					for (size_t k = 0; k < nOp; k++){
						if(maqs[i].reqs[k] == true &&
							ops[j].habs[k] == false){
							capaz = false;
							break;
						}
					}

					if (capaz){
						ops[j].disp = false;
						maqs[i].uso[t] = true;
						asig[iAsig].op = &ops[j];
						asig[iAsig].maq = &maqs[i];
						asig[iAsig].diurno = t;
						iAsig++;
					}
				}
			}
		}
	}

	return iAsig;
}
