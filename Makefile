CC = gcc
CFLAGS = -std=c17 -pedantic -Wall -Wextra -g

lecheria: lecheria.c
	$(CC) $(CFLAGS) -o lecheria lecheria.c

clean:
	rm -v lecheria
